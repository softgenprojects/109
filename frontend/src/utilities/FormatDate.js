import { format } from 'date-fns';

export const FormatDate = (date) => {
  return format(date, 'MMMM dd, yyyy');
};

export const FormatDateTime = (dateTime) => {
  return format(dateTime, 'MMMM dd, yyyy HH:mm');
};

export const FormatTime = (time) => {
  return format(time, 'HH:mm');
};