import { Box, VStack, Heading, Text, Button, useColorModeValue, Center, Container, Flex, Icon } from '@chakra-ui/react';
import { Header } from '@components/Header';
import { Footer } from '@components/Footer';
import { usePathFinderData } from '@hooks/usePathFinderData';
import { FaFacebook } from 'react-icons/fa';

const Home = () => {
  const bgColor = useColorModeValue('gray.50', 'gray.800');
  const textColor = useColorModeValue('gray.800', 'whiteAlpha.900');
  const { data, error, isLoading } = usePathFinderData();

  if (isLoading) return <Center>Loading...</Center>;
  if (error) return <Center>Error loading data</Center>;

  return (
    <Box bg={bgColor} color={textColor} minH="100vh">
      <Header />
      <Container maxW='container.xl' py='12'>
        <VStack spacing='8'>
          <Heading as='h1' size='2xl' textAlign='center' color='purple.600'>
            Teachers use Pathfinder to spark the curiosity of their learners
          </Heading>
          <Text fontSize='lg' textAlign='center' px={{ base: '4', md: '20' }}>
            PathFinder is the easiest way to implement project-based and self-directed learning...
          </Text>
          <Flex gap='4' direction={{ base: 'column', md: 'row' }} align='center' justify='center'>
            <Button leftIcon={<Icon as={FaFacebook} />} colorScheme='purple' variant='solid'>
              Join our group
            </Button>
            <Button colorScheme='purple' variant='outline'>
              Book a demo
            </Button>
          </Flex>
        </VStack>
      </Container>
      <Footer />
    </Box>
  );
};

export default Home;