import React from 'react';
import { Box, VStack, Link, Text, Image } from '@chakra-ui/react';

export const Footer = () => {
  const footerLinks = [
    { name: 'About', href: '#' },
    { name: 'Contact', href: '#' },
    { name: 'Terms & Conditions', href: '#' },
    { name: 'Privacy Policy', href: '#' },
    { name: 'Cookies Policy', href: '#' }
  ];

  return (
    <Box as='footer' bg='purple.700' color='white' py={{ base: '6', md: '10' }} px={{ base: '4', md: '8' }}>
      <VStack spacing='4' align='stretch'>
        {footerLinks.map((link) => (
          <Link key={link.name} href={link.href} fontWeight='bold' color='white'>
            {link.name}
          </Link>
        ))}
      </VStack>
      <Box align='center' mt='8'>
        <Image src='https://i.pravatar.cc/300' alt='Footer Logo' boxSize='50px' borderRadius='full' />
        <Text fontSize='sm' mt='2'>© 2023 PathFinder. All rights reserved.</Text>
      </Box>
    </Box>
  );
};