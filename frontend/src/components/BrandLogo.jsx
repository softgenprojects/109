import { Text, HStack, Image } from '@chakra-ui/react';

export const BrandLogo = ({ logoUrl, altText, size = '32px' }) => (
  <HStack spacing={2} alignItems="center">
    <Image src={logoUrl} alt={altText} boxSize={size} objectFit="cover" borderRadius="full" border="2px" borderColor="purple.500" />
    <Text fontSize="xl" fontWeight="bold" color="purple.600">
      Pathfinder
    </Text>
  </HStack>
);