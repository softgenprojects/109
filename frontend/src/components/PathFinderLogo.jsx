import React from 'react';

export const PathFinderLogo = () => {
  return (
    <img
      src='https://placehold.co/200x50?text=PathFinder+Logo'
      alt='PathFinder Logo'
      style={{ width: '100%', height: 'auto' }}
    />
  );
};