import { Box, Flex, Spacer, Link } from '@chakra-ui/react';
import { BrandLogo } from '@components/BrandLogo';

export const Header = () => {
  const navLinks = [
    { name: 'About', href: '#' },
    { name: 'Contact', href: '#' },
    { name: 'Terms & Conditions', href: '#' },
    { name: 'Privacy Policy', href: '#' },
    { name: 'Cookies Policy', href: '#' }
  ];

  return (
    <Box as='header' bg='white' p={4} boxShadow='sm' position='fixed' width='full' zIndex='docked'>
      <Flex align='center' justify='space-between' maxW='1200px' mx='auto'>
        <BrandLogo logoUrl='https://i.pravatar.cc/32' altText='PathFinder Logo' />
        <Spacer />
        <Flex gap={4}>
          {navLinks.map((link) => (
            <Link key={link.name} href={link.href} p={2} fontWeight='bold' color='purple.600'>
              {link.name}
            </Link>
          ))}
        </Flex>
      </Flex>
    </Box>
  );
};